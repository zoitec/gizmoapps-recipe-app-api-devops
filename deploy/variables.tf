variable "prefix" {
  default = "graad"
}

variable "project" {
  default = "gizmoapps-recipe-app-api-devops"
}

variable "contact" {
  default = "seandjoseph@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "gizmoapps-recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "258224333206.dkr.ecr.us-east-1.amazonaws.com/gizmoapps-recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "258224333206.dkr.ecr.us-east-1.amazonaws.com/gizmoapps-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "vvbuzz.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}



